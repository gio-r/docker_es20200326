FROM python:latest

COPY ./app/ /var/webapp/

WORKDIR /var/webapp
RUN pip install -r ./requirements.txt

CMD [ "index.py" ]
ENTRYPOINT [ "python" ]