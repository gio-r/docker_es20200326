from flask import Flask, render_template
from flaskext.mysql import MySQL
import os

app = Flask(__name__)
mysql = MySQL()
 
#MySQL configurations
app.config['MYSQL_DATABASE_USER'] = os.environ['DB_USER']
app.config['MYSQL_DATABASE_PASSWORD'] = os.environ['PASSWORD']
app.config['MYSQL_DATABASE_DB'] = os.environ['DATABASE']
app.config['MYSQL_DATABASE_HOST'] = os.environ['DB_HOST']
mysql.init_app(app)

# set conn to None: at this point it's possible the database is not yet ready to receive connections
conn = None

@app.route("/")
def main():
    # data that will be extracted from the database and inserted in the HTML file
    db_data = None
    global conn
    if (conn is None):
        # a connection to the database has yet to be established
        # try to connect to database
        try:
            conn = mysql.connect()
        except Exception as e:
            db_data = e
    if (conn is not None):
        # a connection to the database was previously established
        cursor = conn.cursor()
        db_data = cursor.execute("SELECT num FROM tab")
    # whether the data was successfully recovered from the database or not, the HTML page must be sent to whoever requested it
    return render_template('index.html', data=str(db_data))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)